# Mis arreglos para PHP.

A lo largo de los años he juntado un montón de cosas que uso siempre para "arreglar" PHP y este repositorio es donde medio pienso acomodarlos.

## Clases

### DDBB/MariaDB

Clase que permite conectarse a una base de datos MariaDB mediante un único argumento similar a una URL y realizar consultas obteniendo una respuesta consistente sin importar el tipo de consulta. Además permite obtener rápidamente la estructura de una tabla.

## Funciones

### arrBiDimToAsciiTable

Imprime y/o devuelve un string con una tabla ASCII para un array bidimensional

Argumentos:

* `$array`, array bidimensional a convertir.
* `$titleFields`, array asociativo de campos y titulos a mostrar, permite ignorar algún campo definiendo los que se deben mostrar, si esta vacio se usan las claves del primer elemento de $array.
* `$markdownCompat`, booleano que indica si la salida debe ser compatible con MarkDown (no agregar cierres a la tabla).
* `$echo`, booleano que indica si la salida se imprime o no (siempre se devuelve el string creado).

Ejemplo:

``` [PHP]
$data = [
	[
		'id' => 198,
		'qty' => 1,
		'price' => 1000,
	],
	[
		'id' => 199,
		'qty' => 1,
		'price' => 558,
	],
];

arrBiDimToAsciiTable($data);
// imprime:
// +------+-----+-------+
// | id   | qty | price |
// |------|-----|-------|
// | 198  | 1   | 1000  |
// | 199  | 1   | 558   |
// +------+-----+-------+

arrBiDimToAsciiTable($data, ['id' => '#', 'price' => 'Price']);
// imprime:
// +------+-------+
// | #    | Price |
// |------|-------|
// | 198  | 1000  |
// | 199  | 558   |
// +------+-------+

arrBiDimToAsciiTable($data, [], true);
// imprime:
// | id   | qty | price |
// |------|-----|-------|
// | 198  | 1   | 1000  |
// | 199  | 1   | 558   |
```

### arrayToXML

Genera un XML (o HTML) en base a un array de nodos.

`$array` se procesa asi:

* el valor del elemento con indice cero (0) es el nombre elemento XML
* los valores de los elementos con inices numericos (distinto de cero) o asociativos iniciados con el signo igual (=) son los elementos XML hijos del elemento
* las claves de los elemento con indices asociativos (no iniciadas con "=") son los nombres de los atributos del elemento XML, y sus valores son los valores correspondientes (siempre convertidos en strings, si es un array se une con espacios entre elemento y elemento ignorando sus claves), si la clave es la cadena vacia se omite ese valor (esto sirve para por ejemplo los atributos booleanos como el required de los input)
* si un elemento no tiene hijos se considera que es de modelo de contenido vacio, esto se puede evitar añadiendo como un texto hijo la cadena vacia
* si todos los hijos de un elemento son textos no se le añaden sangrias
* estas reglas se aplican recursivamente

Ejemplo:

``` [PHP]
array('div',
	'class'     => 'ejemplo',
	'data-test' => 1,
	'elemento hijo de solo texto',
	array('b',
		'id' => 's',
		'contenido del XML hijo',
		'mas contenido (sin espacio antes)',
	),
	'otro elemento hijo de solo texto',
	array('span',
		'title' => 'vacio',
		'', // esta linea es para evitar un <span title="vacio"/>
	),
	array('img',
		'src' => 'imagen.png',
		'alt' => 'elemeto con modelo de contenido vacio',
		''    => 'este atributo no se mostrara',
	)
)
```

Resulta en:

``` [XML]
<div class="ejemplo" data-test="1">
	elemento hijo de solo texto
	<b id="">contenido del XML hijomas contenido (sin espacio antes)</b>
	otro elemento hijo de solo texto
	<span title="vacio"></span>
	<img src="imagen.png" alt="elemeto con modelo de contenido vacio"/>
</div>
```

Argumentos:

* `$array`, Array a procesar
* `$return`, true: retornar o false: imprimir
* `$xml_declaration`, Cabecera XML
* `$indentation`, ¿Aplicar saltos y sangrias al XML?

### flexibilizeArguments

Función que permite homogeneizar objetos o arrays asociativos a un objeto o array asociativo plantilla. Fue pensada para permitir que las funciones reciban un objeto o array asociativo como único argumento con los nombres de las propiedades o claves admitiendo ligeras diferencias con los deseados (mayúsculas y caracteres no alfanuméricos) y en cualquier orden.

Argumentos:

* `$definitions`, objeto estándar (`stdClass`) o array asociativo con los valores por defecto.
* `$received`, objeto estándar o array asociativo con los valores recibidos.
* `$return_as_object`, booleano que indica si debe devolverse como un objeto estándar o un array asociativo.

Ejemplo:

``` [PHP]
// Sin usar flexibleArguments

function old_buscar (
	$tabla       = 'clientes',
	$condiciones = [],
	$por_pagina  = 10
)
{
	// implementación de "buscar" accediendo a los argumentos mediante:
	// * $tabla
	// * $condiciones
	// * $por_pagina
}

old_buscar('clientes', ['idcliente' => 1, 'activo' => 1], 20);

// Usando flexibleArguments

function buscar (array $a = [])
{
	$a = flexibleArguments(
		[
			'tabla'       => 'clientes',
			'condiciones' => [],
			'por_pagina'  => 10,
		],
		$a
	);
	// implementación de "buscar" accediendo a los argumentos mediante:
	// * $a->tabla
	// * $a->condiciones
	// * $a->por_pagina
}

buscar([
	'porpagina'   => 50,
	'Condiciones' => ['idcliente' => 1, 'activo' => 1],
]);

```

### joinNotEmpty

Elimina los elementos "vacios" de un array de escalares y luego los concatena usando un separador similar a implode.

Ejemplo:

``` [PHP]
joinNotEmpty([1, 2, 3, null, 4, '', 5, 0, false, '0']) // '1,2,3,4,5,0,0'
```

Argumentos:

* `$list`, Array a procesar
* `$glue`, String a usar entre los elementos unidos, por defecto es una coma (`,`)
* `$empty`, Array de valores considerados "vacios", por defecto es `[null, false, '']`

### mergeArrays

Combina recursivamente dos arrays, pero si encuentra la misma clave no numérica y el valor no es un array solo deja el valor del segundo array.

``` [PHP]
/* mergeArrays(
	[
		'zero',
		'one',
		'two' => 2,
		'a' => 'alpha',
		'o' => new stdClass(),
		'l' => [1, 2, 3],
	],
	[
		'cero',
		'uno',
		'dos' => 2,
		'a' => 'alfa',
		'o' => 123,
		'l' => [4, 5, 6],
	]
) = [
	0 => 'zero',
	1 => 'one',
	'two' => 2,	
	'a' => 'alfa',             // array_merge_recursive: ['alpha', 'alfa']
	'o' => 123,                // array_merge_recursive: [123]
	'l' => [1, 2, 3, 4, 5, 6], // array_merge: [4, 5, 6]
	2 => 'cero',
	3 => 'uno',
	'dos' => 2,
] */
```

### removeRecursion

Remover recursión en un objeto o array

Si la variable ingresada ($var) no es un objeto o un array devolverá la variable ingresada. Si la variable ingresada es un objeto o un array devolverá un array con el contenido de esa variable sin recursividad. Para esto concatena las claves o nombres de propiedades (mediante $keysGlue).

Se puede forzar que devuelva una determinada propiedad como null si esta no existe mediante $addNulls.

También se puede elegir las claves que devolverá mediante el array $props. Si un elemento de este array tiene indice numérico su valor se considerara como una propiedad o clave de $var, si el elemento es tiene una clave asociativa se interpreta que esta clave es clave o propiedad de $var y el valor debe ser un sub-array con claves o propiedades para el valor de esa clave o propiedad en $var.

Argumentos:

* `$var`, variable a procesar.
* `$props`, array de propiedades o claves a extraer.
* `$keysGlue`, texto de unión entre niveles de claves y/o propiedades.
* `$addNulls`, booleano que indica si se deben añadir claves o propiedades solicitadas que no existan como null.

Ejemplo:

``` [PHP]
$order = (object)[
	'id'       => 159,
	'date'     => '2020-05-25',
	'customer' => (object)[
		'id'      => 123,
		'name'    => 'John Doe',
		'address' => (object)[
			'street'  => 'San Martín 157',
			'city'    => 'Córdoba',
			'country' => 'AR',
		],
	],
	'details'  => [
		(object)[
			'id' => 198,
			'qty' => 1,
			'price' => 1000,
		],
		(object)[
			'id' => 199,
			'qty' => 1,
			'price' => 558,
		],
	],
];

/* removeRecursion($order) = [
	'id'                       => 159,
	'date'                     => '2020-05-25',
	'customer.id'              => 123,
	'customer.name'            => 'John Doe',
	'customer.address.street'  => 'San Martín 157',
	'customer.address.city'    => 'Córdoba',
	'customer.address.country' => 'AR',
	'details.0.id'             => 198,
	'details.0.qty'            => 1,
	'details.0.price'          => 1000,
	'details.1.id'             => 199,
	'details.1.qty'            => 1,
	'details.1.price'          => 558,
] */

/* removeRecursion(
	$order,
	[
		'date',
		'customer',
		'details',
		'total',
	]
) = [
	'date'                     => '2020-05-25',
	'customer.id'              => 123,
	'customer.name'            => 'John Doe',
	'customer.address.street'  => 'San Martín 157',
	'customer.address.city'    => 'Córdoba',
	'customer.address.country' => 'AR',
	'details.0.id'             => 198,
	'details.0.qty'            => 1,
	'details.0.price'          => 1000,
	'details.1.id'             => 199,
	'details.1.qty'            => 1,
	'details.1.price'          => 558,
	'total'                    => null,
] */

/* removeRecursion(
	$order,
	[
		'date',
		'customer' => [
			'id',
			'address' => [
				'country'
			],
		],
	],
	':'
) = [
	'date'                     => '2020-05-25',
	'customer:id'              => 123,
	'customer:address:country' => 'AR',
] */
```

### strToArrNDim

Devuelve un array despues de hacer explodes recursivos para cada separador.

Argumentos:

* `$string`, el string a procesar.
* `$separators`, la lista de separadores a usar.

Ejemplo:

``` [PHP]

strToArrNDim('159,2020-05-25,123|John Doe|San Martín 157&Córdoba&AR,198&1&1000|199&1&558', [',', '|', '&']);
/* retorna:
[
	[
		[
			"159",
		],
	],
	[
		[
			"2020-05-25",
		],
	],
	[
		[
			"123",
		],
		[
			"John Doe",
		],
		[
			"San Martín 157",
			"Córdoba",
			"AR",
		],
	],
	[
		[
			"198",
			"1",
			"1000",
		],
		[
			"199",
			"1",
			"558",
		],
	],
]
*/

```

### transposeArray

Devuelve un nuevo array con el contenido del dado pero con las dos primeras dimensiones intercambiadas.

Argumentos:

* `$array`, array a procesar.

Ejemplo:

``` [PHP]
$a = [
	[1, 2, 3],
	[4, 5, 6],
];

/* transposeArray($a) tendrá la forma: [
	[1, 4],
	[2, 5],
	[3, 6],
] */

$b = [
	'x' => ['i' => 1, 'j' => 2, 'k' => 3],
	'y' => ['i' => 4, 'j' => 5, 'k' => 6],
];

/* transposeArray($b) tendrá la forma: [
	'i' => ['x' => 1, 'y' => 4],
	'j' => ['x' => 2, 'y' => 5],
	'k' => ['x' => 3, 'y' => 6],
] */
```
