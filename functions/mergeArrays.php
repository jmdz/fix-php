<?php

/**
 * fix-PHP
 *
 * My fixes for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2021, Mauro Daino
 *
 * @package     fix-PHP
 * @author      Mauro Daino (jMdZ)
 * @copyright   Copyright (c) 2019, Mauro Daino (http://jmdz.com.ar)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        http://jmdz.com.ar
 * @since       0.1.0
 * @filesource
 */

namespace jMdZ\fix_PHP;

/**
 * Combinar recursivamente dos arrays
 *
 * Similar a array_merge_recursive, pero cuando el elemento no es un array actúa
 * como array_merge.
 *
 * @param   array  $a1  Array a procesar
 * @param   array  $a2  Array a procesar
 *
 * @return  array
 */
function mergeArrays(array $a1, array $a2)
{
    $r = array_merge_recursive($a1);

    foreach ($a2 as $k2 => $v2) {
        if (is_numeric($k2)) {
            $r[] = $v2;
        } elseif (! isset($a1[$k2])) {
            $r[$k2] = $v2;
        } elseif (is_array($a1[$k2]) || is_array($v2)) {
            $r[$k2] = array_merge_recursive(
                (array) $a1[$k2],
                (array) $v2
            );
        } else {
            $r[$k2] = $v2;
        }
    } unset($k2, $v2);

    return $r;
}
