<?php

/**
 * fix-PHP
 *
 * My fixes for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2024, Mauro Daino
 *
 * @package     fix-PHP
 * @author      Mauro Daino (jMdZ)
 * @copyright   Copyright (c) 2024, Mauro Daino (http://jmdz.com.ar)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        http://jmdz.com.ar
 * @since       0.1.0
 * @filesource
 */

namespace jMdZ\fix_PHP;

/**
 * Convertir un string en un array de N dimensiones
 *
 * Devuelve un array despues de hacer explodes recursivos para cada separador.
 *
 * @param   string  $string
 * @param   array   $separators
 *
 * @return  array
 */
function strToArrNDim(string $string, array $separators = [])
{
    return count($separators)
        ? array_map(
            fn($item) => strToArrNDim(
                $item,
                array_slice($separators, 1)
            ),
            explode(current($separators), $string)
        )
        : $string
    ;
}
