<?php

/**
 * fix-PHP
 *
 * My fixes for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2024, Mauro Daino
 *
 * @package     fix-PHP
 * @author      Mauro Daino (jMdZ)
 * @copyright   Copyright (c) 2024, Mauro Daino (http://jmdz.com.ar)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        http://jmdz.com.ar
 * @since       0.1.0
 * @filesource
 */

namespace jMdZ\fix_PHP;

/**
 * Tabla ASCII de un array bidimensional
 *
 * Imprime y/o devuelve un string con una tabla ASCII para un array bidimensional
 *
 * @param   array   $array           array bidimensional a convertir
 * @param   array   $titleFields     array asociativo de campos y titulos a
 *                                   mostrar, permite ignor algn campo definiendo
 *                                   los que se deben mostrar, si esta vacio se
 *                                   usan las claves del primer elemento de
 *                                   $array
 * @param   bool    $markdownCompat  indica si debe ser compatible con MarkDown,
 *                                   si no se requiere la compatibillidad se
 *                                   agregan los cierres a la tabla
 * @param   bool    $echo            indica si se imprime o no, siempre se
 *                                   devuelve el string creado
 *
 * @return  string
 */
function arrBiDimToAsciiTable(
    array $array,
    array $titleFields = [],
    bool $markdownCompat = false,
    bool $echo = true
) {
    if (!$titleFields) {
        $titleFields = array_keys(current($array));
    }

    // calculate sizes

    $sizes = [];

    foreach ($titleFields as $kf => $tf) {
        $sizes[$kf] = strlen($tf);
    } unset($kf, $tf);

    foreach ($array as $item) {
        foreach ($titleFields as $kf => $tf) {
            $sizes[$kf] = max($sizes[$kf], strlen($item[$kf]));
        } unset($kf, $tf);
    } unset($item);

    // output var

    $string = '';

    // open non MD

    if (!$markdownCompat) {
        $string .= '+';

        foreach ($titleFields as $kf => $tf) {
            $string .= '-' . str_pad('', $sizes[$kf], '-') . '-+';
        } unset($kf, $tf);

        $string .= PHP_EOL;
    }

    // titles

    $string .= '|';

    foreach ($titleFields as $kf => $tf) {
        $string .= ' ' . str_pad($tf, $sizes[$kf]) . ' |';
    } unset($kf, $tf);

    $string .= PHP_EOL;

    // separator

    $string .= '|';

    foreach ($titleFields as $kf => $tf) {
        $string .= '-' . str_pad('', $sizes[$kf], '-') . '-|';
    } unset($kf, $tf);

    $string .= PHP_EOL;

    // rows

    foreach ($array as $item) {
        $string .= '|';

        foreach ($titleFields as $kf => $tf) {
            $string .= ' ' . str_pad($item[$kf], $sizes[$kf]) . ' |';
        } unset($kf, $tf);

        $string .= PHP_EOL;
    } unset($item);

    // close non MD

    if (!$markdownCompat) {
        $string .= '+';

        foreach ($titleFields as $kf => $tf) {
            $string .= '-' . str_pad('', $sizes[$kf], '-') . '-+';
        } unset($kf, $tf);

        $string .= PHP_EOL;
    }

    if ($echo) {
        echo $string;
    }

    return $string;
}
