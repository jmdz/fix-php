<?php

/**
 * fix-PHP
 *
 * My fixes for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2020, Mauro Daino
 *
 * @package     fix-PHP
 * @author      Mauro Daino (jMdZ)
 * @copyright   Copyright (c) 2020, Mauro Daino (http://jmdz.com.ar)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        http://jmdz.com.ar
 * @since       0.1.0
 * @filesource
 */

namespace jMdZ\fix_PHP;

/**
 * Remover recursión en un objeto o array
 *
 * Si la variable ingresada ($var) no es un objeto o un array devolverá la
 * variable ingresada. Si la variable ingresada es un objeto o un array devolverá
 * un array con el contenido de esa variable sin recursividad. Para esto
 * concatena las claves o nombres de propiedades (mediante $keysGlue).
 *
 * Se puede forzar que devuelva una determinada propiedad como null si esta no
 * existe mediante $addNulls.
 *
 * También se puede elegir las claves que devolverá mediante el array $props. Si
 * un elemento de este array tiene indice numérico su valor se considerara como
 * una propiedad o clave de $var, si el elemento es tiene una clave asociativa se
 * interpreta que esta clave es clave o propiedad de $var y el valor debe ser un
 * sub-array con claves o propiedades para el valor de esa clave o propiedad en
 * $var.
 *
 * @param   any     $var       Variable a procesar.
 * @param   array   $props     Propiedades o claves a extraer.
 * @param   string  $keysGlue  Texto de unión entre niveles de claves y/o
 *                             propiedades.
 * @param   bool    $addNulls  Indica si se deben añadir claves o propiedades
 *                             solicitadas que no existan como null.
 *
 * @return  any|array
 */
function removeRecursion(
    $var,
    array $props = [],
    string $keysGlue = '.',
    bool $addNulls = true
) {
    if (is_object($var) || is_array($var)) {
        $data = [];

        if ($props) {
            foreach ($props as $k => $v) {
                if (is_string($v)) {
                    if (isset($var->$v)) {
                        if (is_object($var->$v) || is_array($var->$v)) {
                            $ta = removeRecursion(
                                $var->$v,
                                [],
                                $keysGlue,
                                $addNulls
                            );

                            foreach ($ta as $tk => $tv) {
                                $data[$v . $keysGlue . $tk] = $tv;
                            } unset($tk, $tv);
                        } else {
                            $data[$v] = $var->$v;
                        }
                    } elseif ($addNulls) {
                        $data[$v] = null;
                    }
                } elseif (isset($var->$k) || $addNulls) {
                    $ta = removeRecursion(
                        isset($var->$k) ? $var->$k : (object)[],
                        $v,
                        $keysGlue,
                        $addNulls
                    );

                    foreach ($ta as $tk => $tv) {
                        $data[$k . $keysGlue . $tk] = $tv;
                    } unset($tk, $tv);
                }
            } unset($k, $v);
        } else {
            foreach ($var as $k => $v) {
                if (is_object($v) || is_array($v)) {
                    $ta = removeRecursion($v, $props, $keysGlue, $addNulls);

                    foreach ($ta as $tk => $tv) {
                        $data[$k . $keysGlue . $tk] = $tv;
                    } unset($tk, $tv);
                } else {
                    $data[$k] = $v;
                }
            } unset($k, $v);
        }

        return $data;
    } else {
        return $var;
    }
}
