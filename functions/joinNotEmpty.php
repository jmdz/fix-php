<?php

/**
 * fix-PHP
 *
 * My fixes for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2019, Mauro Daino
 *
 * @package     fix-PHP
 * @author      Mauro Daino (jMdZ)
 * @copyright   Copyright (c) 2019, Mauro Daino (http://jmdz.com.ar)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        http://jmdz.com.ar
 * @since       0.1.0
 * @filesource
 */

namespace jMdZ\fix_PHP;

/**
 * Implode excluyendo los vacios
 *
 * Elimina los elementos "vacios" de un array de escalares y luego los concatena
 * usando un separador similar a implode.
 *
 * @param   array   $list   Array a procesar
 * @param   string  $glue   String a usar entre los elementos unidos
 * @param   array   $empty  Array de valores considerados "vacios"
 *
 * @return  string
 */

function joinNotEmpty(
    array $list,
    string $glue = ',',
    array $empty = [null, false, '']
) {
    return implode(
        $glue,
        array_filter(
            $list,
            fn($v) => is_scalar($v) && ! in_array($v, $empty, true)
        )
    );
}
