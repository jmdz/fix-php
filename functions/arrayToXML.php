<?php

/**
 * fix-PHP
 *
 * My fixes for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2022, Mauro Daino
 *
 * @package     fix-PHP
 * @author      Mauro Daino (jMdZ)
 * @copyright   Copyright (c) 2022, Mauro Daino (http://jmdz.com.ar)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        http://jmdz.com.ar
 * @since       0.1.0
 * @filesource
 */

namespace jMdZ\fix_PHP;

/**
 * Genera un XML (o HTML) en base a un array de nodos.
 *
 * $array se procesa asi:
 *   + el valor del elemento con indice cero (0) es el nombre elemento XML
 *   + los valores de los elementos con inices numericos (distinto de cero) o
 *     asociativos iniciados con el signo igual (=) son los elementos XML hijos
 *     del elemento
 *   + las claves de los elemento con indices asociativos (no iniciadas con "=")
 *     son los nombres de los atributos del elemento XML, y sus valores son los
 *     valores correspondientes (siempre convertidos en strings, si es un array
 *     se une con espacios entre elemento y elemento ignorando sus claves), si la
 *     clave es la cadena vacia se omite ese valor (esto sirve para por ejemplo
 *     los atributos booleanos como el required de los input)
 *   + si un elemento no tiene hijos se considera que es de modelo de contenido
 *     vacio, esto se puede evitar añadiendo como un texto hijo la cadena vacia
 *   + si todos los hijos de un elemento son textos no se le añaden sangrias
 *   + estas reglas se aplican recursivamente
 *   + ejemplo:
 *     array('div',
 *       'class'     => 'ejemplo',
 *       'data-test' => 1,
 *       'elemento hijo de solo texto',
 *       array('b',
 *         'id' => 's',
 *         'contenido del XML hijo',
 *         'mas contenido (sin espacio antes)',
 *       ),
 *       'otro elemento hijo de solo texto',
 *       array('span',
 *         'title' => 'vacio',
 *         '', // esta linea es para evitar un <span title="vacio"/>
 *       ),
 *       array('img',
 *         'src' => 'imagen.png',
 *         'alt' => 'elemeto con modelo de contenido vacio',
 *         ''    => 'este atributo no se mostrara',
 *       )
 *     )
 *     <div class="ejemplo" data-test="1">
 *       elemento hijo de solo texto
 *       <b id="">contenido del XML hijomas contenido (sin espacio antes)</b>
 *       otro elemento hijo de solo texto
 *       <span title="vacio"></span>
 *       <img src="imagen.png" alt="elemeto con modelo de contenido vacio"/>
 *     </div>
 *
 * @param   array    $array             Array a procesar
 * @param   bool     $return            true: retornar o false: imprimir
 * @param   string   $xml_declaration   Cabecera XML
 * @param   bool     $indentation       ¿Aplicar saltos y sangrias al XML?
 *
 * @return  string|null
 */
function arrayToXML(
    array $array,
    bool $return = false,
    string $xml_declaration = '<?xml version="1.0" encoding="utf-8"?>',
    bool $indentation = true
) {
    $xml_declaration = (string) $xml_declaration;

    $xml_declaration = $xml_declaration
        ? $xml_declaration . ($indentation ? "\n" : '')
        : ''
    ;

    $internal_function = function(
        array $array,
        int $tabs_count,
        bool $indentation,
        callable $internal_function
    ) {
        if ($indentation) {
            $indentation_tabs = "\t";
            $indentation_line = "\n";
        } else {
            $indentation_tabs = '';
            $indentation_line = '';
        }

        $output = '';

        if (@$array[0]) {
            $element = $array[0];
            unset($array[0]);

            $attributes = array();
            $children = array();
            $all_text = true;

            foreach ($array as $key => $value) {
                if (! is_numeric($key) && @$key[0] !== '=') {
                    if ($key !== '') {
                        $attributes[$key] = $value;
                    }
                } else {
                    $children[] = $value;

                    if ($all_text && is_array($value)) {
                        $all_text = false;
                    }
                }
            } unset($key,$value);

            $output .= str_repeat($indentation_tabs, $tabs_count)
                . '<' . $element
            ;

            ksort($attributes);

            foreach ($attributes as $key => $value) {
                if (is_array($value)) {
                    $value = implode(' ', $value);
                }

                $output .= ' ' . $key . '="' . $value . '"';
            } unset($key,$value);

            if (! $children) {
                $output .= '/>';
            } else {
                $output .= '>';

                foreach ($children as $value) {
                    if (is_array($value)) {
                        $output .= $indentation_line . $internal_function(
                            $value,
                            $tabs_count + 1,
                            $indentation, $internal_function
                        );
                    } elseif ($all_text) {
                        $output .= $value;
                    } else {
                        $output .= $indentation_line
                            . str_repeat($indentation_tabs, $tabs_count + 1)
                            . $value
                        ;
                    }
                } unset($value);

                if (! $all_text) {
                    $output .= $indentation_line
                        . str_repeat($indentation_tabs, $tabs_count)
                    ;
                }

                $output .= '</' . $element .'>';
            }
        }

        return $output;
    };

    if ($array) {
        $output = $xml_declaration .
            $internal_function($array, 0, $indentation, $internal_function)
        ;
    } else {
        $output = '';
    }

    if ($return) {
        return $output;
    } else {
        echo $output;

        return null;
    }
}
