<?php

/**
 * fix-PHP
 *
 * My fixes for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2019, Mauro Daino
 *
 * @package     fix-PHP
 * @author      Mauro Daino (jMdZ)
 * @copyright   Copyright (c) 2019, Mauro Daino (http://jmdz.com.ar)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        http://jmdz.com.ar
 * @since       0.1.0
 * @filesource
 */

namespace jMdZ\fix_PHP;

/**
 * Flexibilizar argumentos
 *
 * Devuelve un objeto estándar o un array asociativo con las propiedades o
 * claves especificadas en $definitions y el valor en $received si esta
 * disponible, de no estar disponible este el valor que tenía en $definitions.
 *
 * El objeto o array devuelto respeta el orden y los nombres de las propiedades
 * o claves dadas en $definitions, pero en $received las claves o propiedades
 * se consideran siempre en minúsculas y eliminando todos los caracteres que no
 * sean letras o números.
 *
 * @param   object|array  $definitions       Plantilla de objeto o array a
 *                                           retornar.
 * @param   object|array  $received          Datos a asignar.
 * @param   bool          $return_as_object  Indica si se debe retornar un
 *                                           objeto o un array asociativo.
 * @param   bool          $partial           Indica si la búsqueda de las claves
 *                                           de $definitions en $received es por
 *                                           la cadena completa o no.
 *
 * @return  object|array
 */
function flexibilizeArguments (
    $definitions,
    $received = [],
    $return_as_object = true,
    $partial = false
) {
    if (is_object($definitions)) {
        $definitions = (array) $definitions;
    }

    if (is_object($received)) {
        $received = (array) $received;
    }

    $return = [];

    $received = array_combine(
        array_map(
            function ($k) {
                return preg_replace('~[^a-z0-9]~', '', strtolower($k));
            },
            array_keys($received)
        ),
        array_values($received)
    );

    $received_keys = array_keys($received);

    foreach ($definitions as $k => $v) {
        $p = preg_replace('~[^a-z0-9]~', '', strtolower($k));

        $x = $partial
            ? current(array_filter($received_keys, function ($rki) use($p) {
                return strpos($rki, $p) !== false;
            }))
            : $p
        ;

        $return[$k] = isset($received[$x]) ? $received[$x] : $v;

        unset($p);
    } unset($k, $v);

    if ($return_as_object) {
        $return = (object) $return;
    }

    return $return;
}
