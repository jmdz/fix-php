<?php

/**
 * fix-PHP
 *
 * My fixes for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2019, Mauro Daino
 *
 * @package     fix-PHP
 * @author      Mauro Daino (jMdZ)
 * @copyright   Copyright (c) 2019, Mauro Daino (http://jmdz.com.ar)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        http://jmdz.com.ar
 * @since       0.1.0
 * @filesource
 */

namespace jMdZ\fix_PHP;

/**
 * Transponer array
 *
 * Devuelve un nuevo array con el contenido del dado pero con las dos primeras
 * dimensiones intercambiadas.
 *
 * @param   array  $array  Array a procesar
 *
 * @return  array
 */
function transposeArray($array)
{
    $return = [];

    foreach ($array as $a => $x) {
        foreach ($x as $b => $y) {
            if (! isset($return[$b])) {
                $return[$b] = array();
            }

            $return[$b][$a] = $y;
        } unset($b, $y);
    } unset($a, $x);

    return $return;
}
