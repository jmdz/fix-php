<?php

/**
 * fix-PHP
 *
 * My fixes for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2019, Mauro Daino
 *
 * @package     fix-PHP
 * @author      Mauro Daino (jMdZ)
 * @copyright   Copyright (c) 2019, Mauro Daino (http://jmdz.com.ar)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        http://jmdz.com.ar
 * @since       0.1.0
 * @filesource
 */

namespace jMdZ\DDBB;

/**
 * Clase de conexión a la base de datos MariaDB
 *
 * Esta clase permite una rápida conexión a una base de datos MariaDB y provee
 * los métodos para ejecutar consultas, escapar cadenas de texto y describir
 * tablas (además de los correspondientes a la configuración de si misma).
 */
class MariaDB
{
    /**
     * Distintos niveles de historial
     *
     * El nivel guarda:
     *  - SAVE_HISTORY_NON: nada
     *  - SAVE_HISTORY_ERR: errores
     *  - SAVE_HISTORY_ALL: todo
     */
    const SAVE_HISTORY_NON = 0;
    const SAVE_HISTORY_ERR = 1;
    const SAVE_HISTORY_ALL = 2;

    /**
     * El string de conexión actual
     *
     * Debe tener una de las siguientes formas:
     *  - user:pass@host:port/base
     *  - user@host:port/base
     *  - user:pass@host/base
     *  - user@host/base
     *
     * @var  string  $connection
     */
    private $connection = 'root@localhost:3306/test';

    /**
     * El conjunto de caracteres a usar
     *
     * @var  string  $charset
     */
    private $charset = 'utf8';

    /**
     * El huso horario a usar
     */
    private $timezone = '+00:00';

    /**
     * Conexión actual a la base de datos
     *
     * @var  object|resource  $link
     */
    private $link = null;

    /**
     * Servidor de base de datos
     *
     * @var  string  $host
     */
    private $host = 'localhost';

    /**
     * Puerto del servidor de base de datos
     *
     * @var  int  $port
     */
    private $port = 3306;

    /**
     * Usuario del servidor de base de datos
     *
     * @var  string  $user
     */
    private $user = 'root';

    /**
     * Contraseña del usuario del servidor de base de datos
     *
     * @var  string  $pass
     */
    private $pass = null;

    /**
     * Nombre de la base de datos
     *
     * @var  string  $base
     */
    private $base = 'test';

    /**
     * Historial de consultas
     */
    private $history = [];

    /**
     * Constructor de la conexión
     *
     * @param   string       $connection  El string de conexión, de la forma
     *                                    indicada en la propiedad $connection
     * @param   string|null  $charset     El conjunto de caracteres a usar, si
     *                                    es NULL se usa el valor de la
     *                                    propiedad $charset
     * @param   string|null  $timezone    El huso horario a usar, si es NULL se
     *                                    usa el valor de la propiedad $timezone.
     *
     * @return  void
     */
    public function __construct($connection, $charset = null, $timezone = null)
    {
        $this->connection = $connection;
        if ($charset) {
            $this->charset = $charset;
        }
        if ($timezone) {
            $this->timezone = $timezone;
        }
        $analysis = parse_url('http://' . $connection);
        if (isset($analysis['host'])) {
            $this->host = $analysis['host'];
        }
        if (isset($analysis['user'])) {
            $this->user = $analysis['user'];
        }
        if (isset($analysis['pass'])) {
            $this->pass = $analysis['pass'];
        }
        if (isset($analysis['port'])) {
            $this->port = $analysis['port'];
        }
        if (isset($analysis['path'])) {
            $this->base = substr($analysis['path'], 1);
        }
        $this->link = mysqli_connect(
            $this->host,
            $this->user,
            $this->pass,
            $this->base,
            $this->port
        );
        if ($this->link) {
            mysqli_set_charset($this->link, $this->charset);
            mysqli_query($this->link, "SET time_zone='{$this->timezone}'");
        }
    }

    /**
     * Pseudo-alias del constructor
     *
     * @param   string       $connection  El string de conexión, de la forma
     *                                    indicada en la propiedad $connection
     * @param   string|null  $charset     El conjunto de caracteres a usar, si
     *                                    es NULL se usa el valor de la
     *                                    propiedad $charset
     * @param   string|null  $timezone    El huso horario a usar, si es NULL se
     *                                    usa el valor de la propiedad $timezone.
     *
     * @return  self
     */
    public static function connect($connection, $charset = null, $timezone = null)
    {
        return new static($connection, $charset, $timezone);
    }

    /**
     * Método mágico para var_dump que protege la contraseña
     *
     * @return  array  Para var_dump
     */
    public function __debugInfo()
    {
        $class = static::class;
        return [
            "connection:$class:private" => $this->user
                . ( $this->pass !== null ? ":*********" : "" )
                . "@{$this->host}"
                . ( $this->port ? ":{$this->port}" : $this->port )
                . "/{$this->base}"
            ,
            "charset:$class:private" => $this->charset,
            "timezone:$class:private" => $this->timezone,
            "host:$class:private"    => $this->host,
            "port:$class:private"    => $this->port,
            "user:$class:private"    => $this->user,
            "pass:$class:private"    => "*********",
            "base:$class:private"    => $this->base,
            "history:$class:private" => $this->history,
            "link:$class:private"    => $this->link,
        ];
    }

    /**
     * Ejecutor de consultas a la base de datos
     *
     * Permite enviar cualquier consulta al servidor de base de datos y siempre
     * devuelve un objeto con las siguientes propiedades:
     *  - sql        string    el parámetro $sql
     *  - status     bool      booleano de estado
     *  - error      string    texto del último error
     *  - error_num  int       código del último error
     *  - quantity   int       cantidad de resultados
     *  - results    array     resultados (objetos o arrays asociativos según el
     *                         valor del parámetro $as_object)
     *  - last_id    null|int  id generado por la última consulta "insert"
     *
     * @param   string  $sql           El código SQL a ejecutar
     * @param   bool    $as_object     Indica si los resultados (si los hay)
     *                                 deben ser objetos o arrays asociativos
     * @param   int     $save_history  El nivel de guardado del historial, los
     *                                 valores validos estan definidos en las
     *                                 constantes SAVE_HISTORY_*
     *
     * @return  object
     */
    public function query($sql, $as_object = true, $save_history = self::SAVE_HISTORY_NON)
    {
        $return = (object) [
            'sql'       => $sql,
            'status'    => null,
            'error'     => null,
            'error_num' => null,
            'quantity'  => null,
            'results'   => [],
            'last_id'   => null,
        ];
        $result = mysqli_query($this->link, $sql);
        if ($result) {
            $return->status = true;
            if ($result === true) {
                $return->quantity = mysqli_affected_rows($this->link);
                $return->last_id  = mysqli_insert_id($this->link);
            } else {
                $return->quantity = mysqli_num_rows($result);
                $method =
                    $as_object
                    ? 'mysqli_fetch_object'
                    : 'mysqli_fetch_assoc'
                ;
                while ($row = call_user_func($method, $result)) {
                    $return->results[] = $row;
                }
                unset($row);
                unset($method);
                mysqli_free_result($result);
            }
            if ($save_history >= static::SAVE_HISTORY_ALL) {
                $this->history[] = $return;
            }
        } else {
            $return->status    = false;
            $return->error     = mysqli_error($this->link);
            $return->error_num = mysqli_errno($this->link);
            if ($save_history >= static::SAVE_HISTORY_ERR) {
                $this->history[] = $return;
            }
        }
        return $return;
    }

    /**
     * Pseudo-alias de query
     *
     * @param   string  $sql           El código SQL a ejecutar
     * @param   bool    $as_object     Indica si los resultados (si los hay)
     *                                 deben ser objetos o arrays asociativos
     * @param   int     $save_history  El nivel de guardado del historial, los
     *                                 valores validos estan definidos en las
     *                                 constantes SAVE_HISTORY_*
     *
     * @return  object
     */
    public function __invoke($sql, $as_object = true, $save_history = self::SAVE_HISTORY_NON)
    {
        return $this->query($sql, $as_object, $save_history);
    }

    /**
     * Escapa los caracteres especiales de una cadena según la propiedad $charset
     *
     * @param   string  $string  cadena a procesar
     *
     * @return  string
     */
    public function escape($string)
    {
        return mysqli_real_escape_string($this->link, $string);
    }

    /**
     * Devuelve el valor de la propiedad homónima
     *
     * @return  string
     */
    public function connection()
    {
        return $this->connection;
    }

    /**
     * Devuelve el valor de la propiedad homónima
     *
     * @return  string
     */
    public function charset()
    {
        return $this->charset;
    }

    /**
     * Devuelve el valor de la propiedad homónima
     */
    public function timezone()
    {
        return $this->timezone;
    }

    /**
     * Devuelve el valor de la propiedad homónima
     *
     * @return  object|resource
     */
    public function link()
    {
        return $this->link;
    }

    /**
     * Devuelve el valor de la propiedad homónima
     *
     * @return  string
     */
    public function host()
    {
        return $this->host;
    }

    /**
     * Devuelve el valor de la propiedad homónima
     *
     * @return  int
     */
    public function port()
    {
        return $this->port;
    }

    /**
     * Devuelve el valor de la propiedad homónima
     *
     * @return  string
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * Devuelve el valor de la propiedad homónima
     *
     * @return  string
     */
    public function pass()
    {
        return $this->pass;
    }

    /**
     * Devuelve el valor de la propiedad homónima
     *
     * @return  string
     */
    public function base()
    {
        return $this->base;
    }

    /**
     * Devuelve el historial
     */
    public function history()
    {
        return $this->history;
    }

    /**
     * Devuelve el estado de conexión
     *
     * @return  bool
     */
    public function status()
    {
        return $this->link ? true : false;
    }

    /**
     * Devuelve un objeto que describe una tabla
     *
     * Devuelve un objeto con las siguientes propiedades:
     *  - name       string  el parámetro $table
     *  - rows_qty   int     cantidad de filas en la tabla
     *  - cols_qty   int     cantidad de columnas en la tabla
     *  - cols_def   object  las definiciones de cada columna en la tabla
     *
     * Cada columna es definida con un objeto que tiene las siguientes
     * propiedades:
     *  - name       string  nombre de la columna
     *  - type       string  uno de los siguientes: 'BOOLEAN', 'NUMBER', 'DATE',
     *                       'DATETIME-LOCAL', 'DATETIME', 'TIME', 'TEXT',
     *                       'BIT', 'BYTE' o 'LIST'
     *  - null       bool    indica si acepta o no valores NULL
     *  - key        bool    indica si es o no parte de un índice cualquiera
     *  - pri_key    bool    indica si es o no parte de un índice primario
     *  - uni_key    bool    indica si es o no parte de un índice único
     *  - default    mixed   'CURRENT_TIMESTAMP', NULL, o el valor por defecto
     *                       del campo
     *  - auto_val   string  uno de los siguientes: 'CURRENT_TIMESTAMP',
     *                       'AUTO_INCREMENT' o '' (cadena vacía)
     *  - comment    string  texto del comentario del campo
     *
     * Además puede tener otras propiedades según el valor de "type".
     *
     * Si "type" es 'NUMBER', 'DATE', 'DATETIME-LOCAL', o 'DATETIME':
     *  - min        string  valor mínimo que acepta el campo
     *  - max        string  valor máximo que acepta el campo
     *
     * Si "type" es 'NUMBER':
     *  - step       string  fracción mínima que acepta el campo
     *
     * Si "type" es 'TIME', estos campos pueden aceptar la hora de un momento
     * dado o una cantidad de tiempo entre dos momentos, en este último caso
     * estas propiedades indican cuantos días enteros han transcurrido:
     *  - min_hours  string  valor mínimo de horas y minutos que acepta el campo
     *  - max_hours  string  valor máximo de horas y minutos que acepta el campo
     *  - min_days   string  valor mínimo de días enteros que acepta el campo
     *  - max_days   string  valor máximo de días enteros que acepta el campo
     *
     * Si "type" es 'TEXT', 'BIT' o 'BYTE':
     *  - max_len    string  longitud máxima que acepta el campo
     *
     * Si "type" es 'LIST':
     *  - multiple   bool    indica si el campo acepta o no múltiples opciones
     *  - options    array   opciones que acepta el campo, los índices del array
     *                       corresponden al valor interno del motor de base de
     *                       datos
     *
     * @param   string  $table             El nombre de la tabla a analizar
     * @param   bool    $tinyint1_as_bool  Indica si los campos TINYINT(1) se
     *                                     consideran lógicos o enteros
     * @param   bool    $include_raw       Indica si el análisis del los campos
     *                                     debe o no incluir los datos crudos
     *                                     provistos por el motor de base de
     *                                     datos y el pre-procesado de estos
     *
     * @return  object
    */
    public function describeTable(
        $table,
        $tinyint1_as_bool = true,
        $include_raw = false
    ) {
        $return = (object) [
            'name'     => $table,
            'rows_qty' => null,
            'cols_qty' => null,
            'cols_def' => (object) [],
        ];
        $query = $this->query('SELECT COUNT(*) AS `count` FROM `' . $table . '`');
        $return->rows_qty =
            $query->results
            ? + $query->results[0]->count
            : null
        ;
        $query = $this->query('SHOW FULL COLUMNS FROM `' . $table . '`');
        $return->cols_qty = $query->quantity;

        $preprocessType = function ($str_type) {
            $return = (object) [
                'str_type' => $str_type,
                'type'     => null,
                'params'   => [],
                'unsigned' => false,
                'zerofill' => false,
            ];
            if (strpos($str_type, '(') !== false) {
                $return->unsigned = (bool) preg_match(
                    '~\\) UNSIGNED( ZEROFILL$)?$~i',
                    $str_type
                );
                $return->zerofill = (bool) preg_match(
                    '~\\) UNSIGNED ZEROFILL$~i',
                    $str_type
                );
                $str_type = preg_replace(
                    '~^([A-Z\\s]+)\\((.+)\\)( UNSIGNED)?( ZEROFILL)?$~i',
                    '\\1Ð\\2',
                    $str_type
                );
                $str_type = explode('Ð', $str_type, 2);
                $return->type = preg_replace(
                    '~\s+~',
                    '_',
                    strtoupper($str_type[0])
                );
                if (preg_match('~^(SET|ENUM)$~', $return->type)) {
                    $string = false;
                    $acum   = '';
                    $len    = mb_strlen($str_type[1]);
                    $len_m1 = $len - 1;
                    $i      = 0;
                    while ($i < $len) {
                        $char = mb_substr($str_type[1], $i, 1);
                        $next =
                            $i == $len_m1
                            ? ''
                            : mb_substr($str_type[1], $i+1, 1)
                        ;
                        if (! $string && $char=='\'') {
                            $string = true;
                        } elseif ($string && $char=='\\' && $next=='\\') {
                            $acum .= '\\';
                            $i++;
                        } elseif (
                            $string
                            && $next=='\''
                            && ($char=='\'' || $char=='\\')
                        ) {
                            $acum .= '\'';
                            $i++;
                        } elseif ($string && $char=='\'') {
                            $string = false;
                            $return->params[] = $acum;
                            $acum = '';
                        } elseif ($string) {
                            $acum .= $char;
                        }
                        unset($char, $next);
                        $i++;
                    }
                    if (strlen($acum) > 0) {
                        $return->params[] = $acum;
                    }
                    unset($string, $acum, $len, $len_m1, $i);
                } else {
                    $return->params = explode(',', $str_type[1]);
                }
            } else {
                $return->type = strtoupper($str_type);
            }
            return $return;
        };

        $definitions = (object) [];

        $definitions->int = [
            '_max' => function ($max, $fill, $length) {
                return
                    $fill
                    ? min(
                        $max,
                        str_repeat('9', $length)
                    )
                    : $max
                ;
            },
            'TINYINT' => [
                /* unsigned = */ false => [
                                            'min' => '-128',
                                            'max' => '127',
                ],
                /* unsigned = */ true  => [
                                            'min' => '0',
                                            'max' => '255',
                ],
            ],
            'SMALLINT' => [
                /* unsigned = */ false => [
                                            'min' => '-32768',
                                            'max' => '32767',
                ],
                /* unsigned = */ true  => [
                                            'min'=>'0',
                                            'max'=>'65535',
                ],
            ],
            'MEDIUMINT' => [
                /* unsigned = */ false => [
                                            'min' => '-8388608',
                                            'max' => '8388607',
                ],
                /* unsigned = */ true  => [
                                            'min' => '0',
                                            'max' => '16777215',
                ],
            ],
            'INT' => [
                /* unsigned = */ false => [
                                            'min' => '-2147483648',
                                            'max' => '2147483647',
                ],
                /* unsigned = */ true  => [
                                            'min' => '0',
                                            'max' => '4294967295',
                ],
            ],
            'BIGINT' => [
                /* unsigned = */ false => [
                                            'min' => '-9223372036854775808',
                                            'max' => '9223372036854775807',
                ],
                /* unsigned = */ true  => [
                                            'min' => '0',
                                            'max' => '18446744073709551615',
                ],
            ],
            'YEAR' => [
                /* no_cent = */ false => [
                                            'min' => '1901',
                                            'max' => '2155',
                ],
                /* no_cent = */ true  => [
                                            'min' => '0',
                                            'max' => '99',
                ],
            ],
        ];
        $definitions->int['BIT']       = &$definitions->int['TINYINT'];
        $definitions->int['MIDDLEINT'] = &$definitions->int['MEDIUMINT'];
        $definitions->int['INTEGER']   = &$definitions->int['INT'];
        $definitions->int['INT1']      = &$definitions->int['TINYINT'];
        $definitions->int['INT2']      = &$definitions->int['SMALLINT'];
        $definitions->int['INT3']      = &$definitions->int['MEDIUMINT'];
        $definitions->int['INT4']      = &$definitions->int['INT'];
        $definitions->int['INT8']      = &$definitions->int['BIGINT'];

        $definitions->float = [
            'FLOAT' => [
                /* unsigned = */ false => [
                                            'dec' => 7,
                                            'min' => '-3.402823466E+38',
                                            'max' => '3.402823466E+38',
                ],
                /* unsigned = */ true  => [
                                            'dec' => 7,
                                            'min' => '0',
                                            'max' => '3.402823466E+38',
                ],
            ],
            'DOUBLE' => [
                /* unsigned = */ false => [
                                            'dec' => 15,
                                            'min' => '-1.7976931348623157E+308',
                                            'max' => '1.7976931348623157E+308',
                ],
                /* unsigned = */ true  => [
                                            'dec' => 15,
                                            'min' => '0',
                                            'max' => '1.7976931348623157E+308',
                ],
            ],
        ];
        $definitions->float['FLOAT4']           = &$definitions->float['FLOAT'];
        $definitions->float['FLOAT8']           = &$definitions->float['DOUBLE'];
        $definitions->float['DOUBLE_PRECISION'] = &$definitions->float['DOUBLE'];

        $definitions->date = [
            'DATE' => [
                            'min' => '1000-01-01',
                            'max' => '9999-12-31',
            ],
            'TIME' => [
                            'min_hours' => '00:00:00.000',
                            'max_hours' => '23:59:59.000',
                            'min_days' => '-34',
                            'max_days' => '34',
            ],
            'DATETIME' => [
                            'min' => '1000-01-01 00:00:00',
                            'max' => '9999-12-31 23:59:59',
            ],
            'TIMESTAMP' => [
                            'min' => '1970-01-01 00:00:00Z',
                            'max' => '2038-01-19 03:14:07Z',
            ],
        ];

        $definitions->text = [
            'TINYTEXT'   => '255',
            'TEXT'       => '65535',
            'MEDIUMTEXT' => '16777215',
            'LONGTEXT'   => '4294967295',
            'CHAR'       => '255',
            'VARCHAR'    => $this->link->server_version < 50003 ? '255' : '65532',
        ];
        $definitions->text['LONG']                       = &$definitions->text['MEDIUMTEXT'];
        $definitions->text['LONG_VARCHAR']               = &$definitions->text['MEDIUMTEXT'];
        $definitions->text['NATIONAL_CHAR']              = &$definitions->text['CHAR'];
        $definitions->text['NCHAR']                      = &$definitions->text['CHAR'];
        $definitions->text['NATIONAL_VARCHAR']           = &$definitions->text['VARCHAR'];
        $definitions->text['NVARCHAR']                   = &$definitions->text['VARCHAR'];
        $definitions->text['NCHAR_VARCHAR']              = &$definitions->text['VARCHAR'];
        $definitions->text['NATIONAL_CHARACTER_VARYING'] = &$definitions->text['VARCHAR'];
        $definitions->text['NATIONAL_CHAR_VARYING']      = &$definitions->text['VARCHAR'];
        $definitions->text['CHARACTER_VARYING']          = &$definitions->text['VARCHAR'];

        $definitions->blob = [
            'TINYBLOB'   => '255',
            'BLOB'       => '65535',
            'MEDIUMBLOB' => '16777215',
            'LONGBLOB'   => '4294967295',
            'BINARY'     => '255',
            'VARBINARY'  => $this->link->server_version < 50003 ? '255' : '65532',
        ];
        $definitions->blob['LONG_VARBINARY'] = &$definitions->blob['MEDIUMBLOB'];

        foreach ($query->results as $field) {

            $return->cols_def->{$field->Field} = (object) [
                'name'     => $field->Field,
                'type'     => null,
                'null'     => strtoupper($field->Null) === 'YES',
                'key'      => (bool) $field->Key,
                'pri_key'  => strtoupper($field->Key) === 'PRI',
                'uni_key'  => strtoupper($field->Key) === 'UNI',
                'default'  => $field->Default,
                'auto_val' => str_replace(
                    'ON UPDATE ',
                    '',
                    strtoupper($field->Extra)
                ),
                'comment'  => $field->Comment,
            ];

            $preprocessed_type = $preprocessType($field->Type);

            switch ($preprocessed_type->type) {
                case 'BOOLEAN':
                case 'BOOL'   :
                    $return->cols_def->{$field->Field}->type = 'BOOLEAN';
                    break;
                case 'TINYINT':
                    if (
                        $tinyint1_as_bool
                        && $preprocessed_type->params[0] == 1
                    ){
                        $return->cols_def->{$field->Field}->type = 'BOOLEAN';
                        break;
                    }
                    // no break
                case 'SMALLINT' :
                case 'MEDIUMINT':
                case 'INT'      :
                case 'BIGINT'   :
                case 'MIDDLEINT':
                case 'INTEGER'  :
                case 'INT1'     :
                case 'INT2'     :
                case 'INT3'     :
                case 'INT4'     :
                case 'INT8'     :
                    $return->cols_def->{$field->Field}->type = 'NUMBER';
                    $return->cols_def->{$field->Field}->min =
                        $definitions->int
                            [$preprocessed_type->type]
                                [$preprocessed_type->unsigned]
                                    ['min']
                    ;
                    $return->cols_def->{$field->Field}->max =
                        $definitions->int['_max'](
                            $definitions->int
                                [$preprocessed_type->type]
                                    [$preprocessed_type->unsigned]
                                        ['max'],
                            $preprocessed_type->zerofill,
                            $preprocessed_type->params[0]
                        )
                    ;
                    $return->cols_def->{$field->Field}->step = '1';
                    break;
                case 'FLOAT'           :
                case 'DOUBLE'          :
                case 'DOUBLE_PRECISION':
                case 'REAL'            :
                case 'FLOAT4'          :
                case 'FLOAT8'          :
                    if ($preprocessed_type->type === 'REAL') {
                        $mode = $this->query('SELECT @@SESSION.sql_mode AS v');
                        $preprocessed_type->type =
                            strpos(
                                $mode->results ? $mode->results[0]->v : '',
                                'REAL_AS_FLOAT'
                            ) !== false
                            ? 'FLOAT'
                            : 'DOUBLE'
                        ;
                        unset($mode);
                    }
                    if (
                        $preprocessed_type->type === 'FLOAT'
                        && count($preprocessed_type->params) === 1
                        && 24 < $preprocessed_type->params[0]
                    ) {
                        $preprocessed_type->type = 'DOUBLE';
                        unset($preprocessed_type->params[0]);
                    }
                    $return->cols_def->{$field->Field}->type = 'NUMBER';
                    $return->cols_def->{$field->Field}->min =
                        $definitions->float
                            [$preprocessed_type->type]
                                [$preprocessed_type->unsigned]
                                    ['min']
                    ;
                    $return->cols_def->{$field->Field}->max =
                        $definitions->float
                            [$preprocessed_type->type]
                                [$preprocessed_type->unsigned]
                                    ['max']
                    ;
                    $return->cols_def->{$field->Field}->step = ''
                        . '0.'
                        . str_repeat(
                            '0',
                            $definitions->float
                                [$preprocessed_type->type]
                                    [$preprocessed_type->unsigned]
                                        ['dec']
                            - 1
                        )
                        . '1'
                    ;
                    break;
                case 'DECIMAL':
                case 'DEC'    :
                case 'NUMERIC':
                case 'FIXED'  :
                    if (! isset($preprocessed_type->params[0])) {
                        $preprocessed_type->params[0] = 10;
                    }
                    if (! isset($preprocessed_type->params[1])) {
                        $preprocessed_type->params[1] = 0;
                    }
                    $return->cols_def->{$field->Field}->type = 'NUMBER';
                    $return->cols_def->{$field->Field}->max =
                        str_repeat(
                            '9',
                            $preprocessed_type->params[0]
                            - $preprocessed_type->params[1]
                        )
                        . '.'
                        . str_repeat('9', $preprocessed_type->params[1])
                    ;
                    $return->cols_def->{$field->Field}->min =
                        $preprocessed_type->unsigned
                        ? '0'
                        : '-' . $return->cols_def->{$field->Field}->max
                    ;
                    $return->cols_def->{$field->Field}->step =
                        $preprocessed_type->params[1]
                        ? '0.'
                            . str_repeat('0', $preprocessed_type->params[1] - 1)
                            . '1'
                        : '1'
                    ;
                    break;
                case 'DATE':
                    $return->cols_def->{$field->Field}->type = 'DATE';
                    $return->cols_def->{$field->Field}->min =
                        $definitions->date
                            [$preprocessed_type->type]
                                ['min']
                    ;
                    $return->cols_def->{$field->Field}->max =
                        $definitions->date
                            [$preprocessed_type->type]
                                ['max']
                    ;
                    break;
                case 'DATETIME':
                    $return->cols_def->{$field->Field}->type = 'DATETIME-LOCAL';
                    $return->cols_def->{$field->Field}->min =
                        $definitions->date
                            [$preprocessed_type->type]
                                ['min']
                    ;
                    $return->cols_def->{$field->Field}->max =
                        $definitions->date
                            [$preprocessed_type->type]
                                ['max']
                    ;
                    break;
                case 'TIMESTAMP':
                    $return->cols_def->{$field->Field}->type = 'DATETIME';
                    $return->cols_def->{$field->Field}->min =
                        $definitions->date
                            [$preprocessed_type->type]
                                ['min']
                    ;
                    $return->cols_def->{$field->Field}->max =
                        $definitions->date
                            [$preprocessed_type->type]
                                ['max']
                    ;
                    break;
                case 'TIME':
                    $return->cols_def->{$field->Field}->type = 'TIME';
                    $return->cols_def->{$field->Field}->min_hours =
                        $definitions->date
                            [$preprocessed_type->type]
                                ['min_hours']
                    ;
                    $return->cols_def->{$field->Field}->max_hours =
                        $definitions->date
                            [$preprocessed_type->type]
                                ['max_hours']
                    ;
                    $return->cols_def->{$field->Field}->min_days =
                        $definitions->date
                            [$preprocessed_type->type]
                                ['min_days']
                    ;
                    $return->cols_def->{$field->Field}->max_days =
                        $definitions->date
                            [$preprocessed_type->type]
                                ['max_days']
                    ;
                    break;
                case 'YEAR':
                    $return->cols_def->{$field->Field}->type = 'NUMBER';
                    $return->cols_def->{$field->Field}->min =
                        $definitions->int
                            [$preprocessed_type->type]
                                [ $preprocessed_type->params[0] == 2 ]
                                    ['min']
                    ;
                    $return->cols_def->{$field->Field}->max =
                        $definitions->int
                            [$preprocessed_type->type]
                                [ $preprocessed_type->params[0] == 2 ]
                                    ['max']
                    ;
                    $return->cols_def->{$field->Field}->step = '1';
                    break;
                case 'TINYTEXT'                  :
                case 'TEXT'                      :
                case 'MEDIUMTEXT'                :
                case 'LONGTEXT'                  :
                case 'LONG'                      :
                case 'LONG_VARCHAR'              :
                case 'CHAR'                      :
                case 'NATIONAL_CHAR'             :
                case 'NCHAR'                     :
                case 'VARCHAR'                   :
                case 'NATIONAL_VARCHAR'          :
                case 'NVARCHAR'                  :
                case 'NCHAR_VARCHAR'             :
                case 'NATIONAL_CHARACTER_VARYING':
                case 'NATIONAL_CHAR_VARYING'     :
                case 'CHARACTER_VARYING'         :
                    $return->cols_def->{$field->Field}->type = 'TEXT';
                    $return->cols_def->{$field->Field}->max_len =
                        isset($preprocessed_type->params[0])
                        ? min(
                            $preprocessed_type->params[0],
                            $definitions->text[$preprocessed_type->type]
                        )
                        : $definitions->text[$preprocessed_type->type]
                    ;
                    break;
                case 'ENUM':
                    $return->cols_def->{$field->Field}->type = 'LIST';
                    $return->cols_def->{$field->Field}->multiple = false;
                    $return->cols_def->{$field->Field}->options =
                        $preprocessed_type->params
                    ;
                    array_unshift(
                        $return->cols_def->{$field->Field}->options,
                        ''
                    );
                    unset($return->cols_def->{$field->Field}->options[0]);
                    break;
                case 'SET':
                    $return->cols_def->{$field->Field}->type = 'LIST';
                    $return->cols_def->{$field->Field}->multiple = true;
                    $return->cols_def->{$field->Field}->options = array_combine(
                        array_map(
                            function ($e) {
                                return pow(2, $e);
                            },
                            array_keys($preprocessed_type->params)
                        ),
                        array_values($preprocessed_type->params)
                    );
                    break;
                case 'BIT':
                    if ($this->link->server_version < 50003) {
                        $return->cols_def->{$field->Field}->type = 'NUMBER';
                        $return->cols_def->{$field->Field}->min =
                            $definitions->int
                                [$preprocessed_type->type]
                                    [false]
                                        ['min']
                        ;
                        $return->cols_def->{$field->Field}->max =
                            $definitions->int
                                [$preprocessed_type->type]
                                    [false]
                                        ['max']
                        ;
                        $return->cols_def->{$field->Field}->step = '1';
                    } else {
                        $return->cols_def->{$field->Field}->type = 'BIT';
                        $return->cols_def->{$field->Field}->max_len =
                            $preprocessed_type->params[0]
                        ;
                    }
                    break;
                case 'TINYBLOB'      :
                case 'BLOB'          :
                case 'MEDIUMBLOB'    :
                case 'LONGBLOB'      :
                case 'BINARY'        :
                case 'VARBINARY'     :
                case 'LONG_VARBINARY':
                    $return->cols_def->{$field->Field}->type = 'BYTE';
                    $return->cols_def->{$field->Field}->max_len =
                        isset($preprocessed_type->params[0])
                        ? min(
                            $preprocessed_type->params[0],
                            $definitions->blob[$preprocessed_type->type]
                        )
                        : $definitions->blob[$preprocessed_type->type]
                    ;
                    break;
            }

            if ($include_raw) {
                $return->cols_def->{$field->Field}->_raw  = $field;
                $return->cols_def->{$field->Field}->_type = $preprocessed_type;
            }

            unset($preprocessed_type);

        }

        unset($field);

        unset($query,$preprocessType,$definitions);

        return $return;
    }
}
